import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { DataService } from '../services/data.service';
import { map } from 'rxjs/operators';
import { Paginated } from '@feathersjs/feathers';


@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CarsComponent implements OnInit {
  cars$: Observable<any[]>;
  carForm;
  
  constructor(private formBuilder: FormBuilder, private data: DataService) {
    this.cars$ = data.cars$().pipe(
        // our data is paginated, so map to .data
        map((t: Paginated<any>) => t.data)
      );
    
  }

  onSubmit(carData) {
    this.data.addCar( carData.make, carData.model, carData.year, carData.mileage );
  }
  
  deleteCar(make: string, model: string, year: string, mileage: string) {
    console.log(make + model + year + mileage);
    this.data.removeCar(make,model,year,mileage);
  }

  ngOnInit(): void {
    this.carForm = this.formBuilder.group({
      make: ['', [Validators.required] ],
      model: ['', [Validators.required] ],
      year: ['', [Validators.required] ],
      mileage: ['', [Validators.required] ]
    });
  }

}
