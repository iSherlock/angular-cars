import { Injectable } from '@angular/core';
import { Feathers } from './feathers.service';

/**
 *  Abstraction layer for data management.
 */
@Injectable()
export class DataService {
  constructor(private feathers: Feathers) {
  }

  things$() {
    // just returning the observable will query the backend on every subscription
    // using some caching mechanism would be wise in more complex applications
    return (<any>this.feathers // todo: remove 'any' assertion when feathers-reactive typings are up-to-date with buzzard
      .service('things'))
      .watch()
      .find();
  }
  
  cars$() {
    return (<any>this.feathers
    .service('cars'))
    .watch()
    .find();
  }

  addThing(desc: string) {
    if (desc === '') {
      return;
    }

    // feathers-reactive Observables are hot by default,
    // so we don't need to subscribe to make create() happen.
    this.feathers
      .service('things')
      .create({
        desc
      });
  }
  
  addCar(make: string, model: string, year: string, mileage:string ){
    if(make === '' || model === '' || year === '' || mileage === ''){
      return;
    }
    
    this.feathers
    .service('cars')
    .create({
      make,
      model,
      year,
      mileage
    })
  }

  removeCar(make: string, model: string, year: string, mileage:string){
    this.feathers
    .service('cars')
    .find({
      query: {
        make: make,
        model: model,
        year: year,
        mileage: mileage
      }
    })
    .then(car=>{
      
      //.remove did not like just using car.data[0].x for the params
      let make = car.data[0].make
      let id = car.data[0].id
      let model = car.data[0].model
      let year = car.data[0].year
      let mileage = car.data[0].mileage
      
      
      this.feathers
      .service('cars')
      .remove(id, {
        make,
        model,
        year,
        mileage
      })
    })
    .catch(error=> console.log(error))
  }
    
}
